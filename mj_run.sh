#!/bin/sh

JAR_FILES="java-cup-11a.jar:mj-runtime.jar:symboltable.jar"
java -cp .:$JAR_FILES rs.etf.pp1.mj.runtime.Run $1 $2

echo "" # Put extra new line
