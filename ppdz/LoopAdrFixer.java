package ppdz;
import java.util.Stack;

/** 
 * This is just simple wrapup for stack, so we can save loop top addresses.
 * 
 * @author 
 * @version 
 */
class LoopAdrFixer {
    private Stack<Integer> savedTopLoopAdrs;

    public LoopAdrFixer() {
        savedTopLoopAdrs = new Stack<Integer>();
    }

    public void saveLoopTop(int loopTopAddress) {
        savedTopLoopAdrs.push(loopTopAddress);
    }

    public int popCurrentLoopTop() {
        return savedTopLoopAdrs.pop();
    }
}
