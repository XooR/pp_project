package ppdz;
import java_cup.runtime.Symbol;
import rs.etf.pp1.mj.runtime.*;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;

%%

%{
    // ukljucivanje informacije o poziciji tokena
    private Symbol new_symbol(int type) {
        return new Symbol(type, yyline + 1, yycolumn);
    }
    // ukljucivanje informacije o poziciji tokena
    private Symbol new_symbol(int type, Object value) {
        return new Symbol(type, yyline + 1, yycolumn, value);
    }
%}

%cup

/* Exclusive state*/
%xstate COMMENT

%eofval{ 
    return new_symbol(sym.EOF);
%eofval}

%line
%column

%%

" "    {}
"\b"   {}
"\t"   {}
"\r\n" {}
"\n"   {}
"\f"   {}

"new"    { return new_symbol(sym.NEW);    }
"class"  { return new_symbol(sym.CLASS);  }
"final"  { return new_symbol(sym.FINAL);  }
"print"  { return new_symbol(sym.PRINT);  }
"return" { return new_symbol(sym.RETURN); }
"void"   { return new_symbol(sym.VOID);   }
"if"     { return new_symbol(sym.IF);     }
"else"   { return new_symbol(sym.ELSE);   }
"while"  { return new_symbol(sym.WHILE);  }

"?"  { return new_symbol(sym.QUESTION);}
":"  { return new_symbol(sym.TDOTS);   }
"."  { return new_symbol(sym.DOT);     }
"+"  { return new_symbol(sym.PLUS);    }
"-"  { return new_symbol(sym.MINUS);   }
"*"  { return new_symbol(sym.TIMES);   } 
"/"  { return new_symbol(sym.DIV);     }
"="  { return new_symbol(sym.EQUAL);   }
";"  { return new_symbol(sym.SEMI);    }
","  { return new_symbol(sym.COMMA);   }
"("  { return new_symbol(sym.LPAREN);  }
")"  { return new_symbol(sym.RPAREN);  }
"{"  { return new_symbol(sym.LBRACE);  }
"}"  { return new_symbol(sym.RBRACE);  }
"["  { return new_symbol(sym.LSQUARE); }
"]"  { return new_symbol(sym.RSQUARE); }
">"  { return new_symbol(sym.RO, new Integer(Code.gt));      }
">=" { return new_symbol(sym.RO, new Integer(Code.ge));      }
"<"  { return new_symbol(sym.RO, new Integer(Code.lt));      }
"<=" { return new_symbol(sym.RO, new Integer(Code.le));      }
"==" { return new_symbol(sym.RO, new Integer(Code.eq));      }
"!=" { return new_symbol(sym.RO, new Integer(Code.ne));      }


"//"            { yybegin(COMMENT);   }
<COMMENT>.      { yybegin(COMMENT);   }
<COMMENT>"\r\n" { yybegin(YYINITIAL); }
<COMMENT>"\n"   { yybegin(YYINITIAL); }

[0-9]+ {
    return new_symbol(sym.NUMBER, new Integer(yytext()));
    }
([a-z]|[A-Z])[a-z|A-Z|0-9|_]* {
    return new_symbol(sym.IDENT, yytext());
    }
"'"[\040-\176]"'" {
    return new_symbol(sym.CHARCONST, new Character (yytext().charAt(1)));
    }
. {
    System.err.println("Leksicka greska ("+yytext()+") u liniji "+(yyline+1));
  }
