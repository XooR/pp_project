#!/bin/sh

# To ignore all useless warnings
rm() {
    /bin/rm "$@" 2>/dev/null
}

clean_class() {
    # rm all class files, except files that I don't have java code
    for i in $(ls ppdz/*.class | grep -vE "VMException.class|FormatException.class")
    do
        rm $i;
    done
    # rm all backup files
    rm ppdz/*~
}

clean_generated() {
# By cup
    rm ppdz/parser.java;
    rm ppdz/sym.java;
# By jflex
    rm ppdz/Yylex.java;
}

clean_all() {
    clean_class;
    clean_generated;
}

clean_all;

JAR_FILES="java-cup-11a.jar:mj-runtime.jar:symboltable.jar"
# Additional CUP Options
ACO="";

if [ "z_$1" = "z_dump" ]
then
        ACO="-dump";
fi

if [ "z_$1" != "z_clean" ]
then
    java -jar JFlex.jar ppdz/MJlexer.lex || exit 1;
    java -jar java-cup-11a.jar $ACO -destdir ./ppdz ppdz/MJparser.cup || exit 1;

    javac -cp $JAR_FILES ppdz/*.java || exit 1;
    echo "Compiling completed successfully";
else
    echo "Cleaning complete";
fi
