#!/bin/sh

JAR_FILES="java-cup-11a.jar:mj-runtime.jar:symboltable.jar"

basefn=`basename $1 .pr`
dirname=`dirname $1`

code_file=$1
obj_file="${dirname}/${basefn}.obj"
# clean, so we don't do dump on prev obj
rm -rf $obj_file 2>/dev/null

echo SOURCE CODE:
cat $code_file
echo "******************************"
echo END OF SOURCE CODE
echo "******************************"

echo COMPILING:
java -cp .:$JAR_FILES ppdz.parser $code_file $obj_file
echo "******************************"
echo END OF COMPILING
echo "******************************"

echo DISASM:
java -cp .:$JAR_FILES rs.etf.pp1.mj.runtime.disasm $obj_file
echo "******************************"
echo END OF DISASM
echo "******************************"

if [ "_$2" == '_run' ];
then
  ./mj_run.sh $obj_file
  echo -e "\n\n"
fi

if [ "_$2" == '_genexp' ];
then
  exp_file="${dirname}/${basefn}.exp"
  ./mj_run.sh $obj_file > $exp_file
fi

if [ "_$2" == '_gencnt' ];
then
  cnt_file="${dirname}/${basefn}.cnt"
  java -cp .:$JAR_FILES ppdz.parser $code_file $obj_file &> $cnt_file
fi
